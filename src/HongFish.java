public class HongFish extends Fish {
  
  private double clownPower;

  public HongFish(int hongHP, double clownPower) {
	super(hongHP, false);
    this.clownPower = clownPower; 
    
  }

  public double getClownPower(){
    return clownPower;
  }
}