public class Clownfish extends Fish {
  
  private double clownLifespan;
  private double clownHunger;
  
  public Clownfish (int clownLifespan, int clownHealth, boolean drought){
	super(clownHealth, drought);
    this.clownLifespan = clownLifespan;
  }
  
  public double getclownLifespan(){
    return clownLifespan;

  }
  public double getclownHunger() {
    return clownHunger;

  }
  
  public void setclownHunger(double clownHunger){
    this.clownHunger = clownHunger;

  }
}