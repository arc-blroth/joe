

public class Fish {
	
	private int fishHP;
	
	public Fish(int fishHP, boolean drought) {
		this.fishHP = fishHP;
	}
	
	public void swim(){
		fishHP -= 3;
	}
	
}
